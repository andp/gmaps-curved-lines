export default {
  createMap: function({ mapElement, center, zoom }) {
    return new Microsoft.Maps.Map(mapElement, {
      zoom: zoom,
      center: center
    });
  },
  createLatLng: function({ lat, lng }) {
    return new Microsoft.Maps.Location(lat, lng);
  },
  createBounds: function({ positions }) {
    return Microsoft.Maps.LocationRect.fromLocations(positions);
  },
  getBoundsCenter: function({ bounds }) {
    const { latitude, longitude } = bounds.center;
    // TODO: reference createLatLng instead
    return new Microsoft.Maps.Location(latitude, longitude);
  },
  fitBounds: function({ map, bounds }) {
    return map.setView({ bounds });
  },
  createMarker: function({ position, label, draggable, map }) {
    const marker = new Microsoft.Maps.Pushpin(position, {
      text: label,
      draggable: draggable
    });
    map.entities.push(marker);
    return marker;
  },

  createDashedLine: function({ path, colour, offset }) {
    return;
  },

  // Math functions
  computeDistanceBetweenPoints: function({ point1, point2 }) {
    return Microsoft.Maps.SpatialMath.getDistanceTo(point1, point2);
  },
  computeHeading: function({ point1, point2 }) {
    return Microsoft.Maps.SpatialMath.getHeading(point1, point2);
  },
  computeOffset: function({ position, distance, lineHeading }) {
    return;
  }
};
