export default {
  createMap: function({ mapElement, center, zoom }) {
    return new google.maps.Map(mapElement, {
      center,
      zoom
    });
  },
  createLatLng: function({ lat, lng }) {
    return new google.maps.LatLng(lat, lng);
  },
  createBounds: function({ positions }) {
    const bounds = new google.maps.LatLngBounds();
    positions.forEach(position => bounds.extend(position));
    return bounds;
  },
  getBoundsCenter: function({ bounds }) {
    return bounds.getCenter();
  },
  fitBounds: function({ map, bounds }) {
    return map.fitBounds(bounds);
  },
  createMarker: function({ position, label, draggable, map }) {
    return new google.maps.Marker({
      position,
      label,
      draggable,
      map
    });
  },

  createDashedLine: function({ path, colour, offset, map }) {
    const dashedLine = offset => ({
      icon: {
        path: 'M 0,-1 0,1',
        strokeColor: colour,
        strokeOpacity: 1,
        scale: 4
      },
      offset: '0',
      repeat: `${offset}px`
    });

    return new google.maps.Polyline({
      path,
      geodesic: true,
      strokeOpacity: 0,
      strokeWeight: 1,
      icons: [dashedLine(offset)]
    }).setMap(map);
  },

  // Math functions
  computeDistanceBetweenPoints: function({ point1, point2 }) {
    return google.maps.geometry.spherical.computeDistanceBetween(
      point1,
      point2
    );
  },
  computeHeading: function({ point1, point2 }) {
    return google.maps.geometry.spherical.computeHeading(point1, point2);
  },
  computeOffset: function({ position, distance, lineHeading }) {
    return google.maps.geometry.spherical.computeOffset(
      position,
      distance,
      lineHeading
    );
  }
};
