import { CONSTANTS } from './constants/constants';
import { mapInterface } from './mapApis';
const { createLatLng, createDashedLine } = mapInterface;
const computations = {
    B1: t => {
      return t * t * t;
    },
    B2: t => {
      return 3 * t * t * (1 - t);
    },
    B3: t => {
      return 3 * t * (1 - t) * (1 - t);
    },
    B4: t => {
      return (1 - t) * (1 - t) * (1 - t);
    }
  },
  getBezier = (coords, percent) => {
    const sumCoords = xOrY =>
      coords
        .map(coord => coord[xOrY])
        .map((arr, index) => arr * computations[`B${index + 1}`](percent))
        .reduce((acc, curr) => acc + curr);

    return {
      x: sumCoords('x'),
      y: sumCoords('y')
    };
  };

export default function CubicBezier(latLong, resolution, map) {
  const points = [],
    path = [];
  const latLngs = latLong.map(coords => ({
    x: coords.lat(),
    y: coords.lng()
  }));

  for (let it = 0; it <= 1; it += resolution) {
    points.push(getBezier(latLngs, it));
  }
  for (let i = 0; i < points.length - 1; i++) {
    path.push(createLatLng({ lat: points[i].x, lng: points[i].y }));
    path.push(createLatLng({ lat: points[i + 1].x, lng: points[i + 1].y }));
  }
  return createDashedLine({ path, colour: CONSTANTS.GREEN, offset: 20, map });
}
