import CubicBezier from './CubicBezier';

import { mapInterface } from './mapApis';
const {
  computeDistanceBetweenPoints,
  computeHeading,
  computeOffset
} = mapInterface;

export default function DrawCurve(point1, point2, map) {
  const curvature = 0.22, // how curvy to make the arc - higher flattens
    lineLength = computeDistanceBetweenPoints({ point1, point2 }),
    lineHeading = computeHeading({ point1, point2 }),
    headingOneAmount = 45,
    headingTwoAmount = 135;
  const calculateHeading = (lineHeading, headingAmount) =>
    lineHeading + (lineHeading < 0 ? headingAmount : headingAmount * -1);
  const computeAllOffset = (position, specificLineHeading) =>
    computeOffset({
      position,
      distance: lineLength / (curvature * 10),
      lineHeading: specificLineHeading
    });
  return new CubicBezier(
    [
      point1,
      computeAllOffset(point1, calculateHeading(lineHeading, headingOneAmount)),
      computeAllOffset(point2, calculateHeading(lineHeading, headingTwoAmount)),
      point2
    ],
    0.01,
    map
  );
}
