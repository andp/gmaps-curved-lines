import DrawCurve from './DrawCurve';
import { mapInterface } from './mapApis';
const {
  createMap,
  createLatLng,
  createBounds,
  fitBounds,
  getBoundsCenter,
  createMarker
} = mapInterface;

const init = () => {
  const mapElement = document.getElementById('map-container');
  // This is the initial location of the points
  const positions = [
      createLatLng({ lat: 31.3049106, lng: 121.4645535 }),
      createLatLng({ lat: 31.2362441, lng: 121.4740914 }),
      createLatLng({ lat: 31.2471577, lng: 121.4845335 }),
      createLatLng({ lat: 31.2309539, lng: 121.4504855 }),
      createLatLng({ lat: 31.3049106, lng: 121.4645535 })
    ],
    bounds = createBounds({ positions });

  const map = createMap({
    mapElement,
    center: getBoundsCenter({ bounds }),
    zoom: 8
  });
  fitBounds({ map, bounds });

  positions.forEach((position, index) => {
    createMarker({
      position,
      label: `${index + 1}`,
      draggable: true,
      map
    });

    index + 1 !== positions.length &&
      new DrawCurve(positions[index + 1], position, map);
  });
};

window.initMap = init;
