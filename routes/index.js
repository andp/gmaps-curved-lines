var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  res.render('index', {
    googleKey: process.env.GOOGLE,
    bingKey: process.env.BING,
    autoNaviKey: process.env.AUTONAVI
  });
});

module.exports = router;
